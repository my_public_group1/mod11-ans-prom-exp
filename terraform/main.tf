provider "aws" {
  region = "us-west-1"
}


resource "aws_eip" "my_web_static_ip" {
  instance = aws_instance.my_webserver.id
  tags = {
    Name  = "WEB Server IP"
  }
}

resource "aws_eip" "my_db_static_ip" {
  instance = aws_instance.my_dbserver.id
  tags = {
    Name  = "MySQL Server IP"
  }
}

resource "aws_eip" "my_mon_static_ip" {
  instance = aws_instance.my_monserver.id
  tags = {
    Name  = "Monitoring Server IP"
  }
}

# Ищем образ с нужной версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

# Запускаем инстанс web
resource "aws_instance" "my_webserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_webserver.id]
  user_data = file("user_data.sh")
  key_name = "aws-iac"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Web Server IP"
    Env = "Production"
  }

  lifecycle {
    create_before_destroy = true
  }

}

# Запускаем инстанс db
resource "aws_instance" "my_dbserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_dbserver.id]
  user_data = file("user_data.sh")
  key_name = "aws-iac"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "MySQL Server IP"
    Env = "Production"
  }

  lifecycle {
    create_before_destroy = true
  }

}

# Запускаем инстанс monitoring
resource "aws_instance" "my_monserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_monserver.id]
  user_data = file("user_data.sh")
  key_name = "aws-iac"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Monitoring Server IP"
    Env = "Production"
  }

  lifecycle {
    create_before_destroy = true
  }

}



resource "aws_security_group" "my_webserver" {
  name        = "Web Security Group"
  description = "Security group for accessing traffic to our Web Server"


  dynamic "ingress" {
    for_each = ["80", "22", "9100", "9113", "9253"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "WEB Server SecurityGroup"
  }
}

resource "aws_security_group" "my_dbserver" {
  name        = "MySQL Security Group"
  description = "Security group for accessing traffic to our MySQL Server"


  dynamic "ingress" {
    for_each = ["3306", "22", "9100", "9104"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "MySQL Server SecurityGroup"
  }
}

resource "aws_security_group" "my_monserver" {
  name        = "Monitoring Security Group"
  description = "Security group for accessing traffic to our Monitoring Server"


  dynamic "ingress" {
    for_each = ["9090", "3000", "9093", "22", "9100", "9094"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    from_port   = 9094
    to_port     = 9094
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Monitoring Server SecurityGroup"
  }
}


# Выведем IP адрес веб сервера
output "my_web_site_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = aws_eip.my_web_static_ip.public_ip
}

# Выведем IP адрес веб сервера
output "my_db_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = aws_eip.my_db_static_ip.public_ip
}

# Выведем IP адрес веб сервера
output "my_monitoring_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = aws_eip.my_mon_static_ip.public_ip
}
